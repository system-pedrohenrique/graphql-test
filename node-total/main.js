require('total4');
const {ROUTE} = require("total4");
const HTTP = require("http");

ROUTE('GET /', function() {
    this.json({ message: 'Hello world' });
});

ROUTE('SOCKET /', function() {
    this.on('open', function(client) {
        client.send({ message: 'Hello' });
    });

    this.on('message', function(client, message) {
        console.log(message);
    });

});

HTTP('debug');
